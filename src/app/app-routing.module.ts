import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  // },
  {
    path: 'main',
    loadChildren: () => import('./main/main.module').then( m => m.MainPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
      path: 'test',
      loadChildren: () => import('./home/test/test.module').then( m => m.TestPageModule)
    },
  {
    path: 'offline-verification',
    loadChildren: () => import('./offline-verification/offline-verification.module').then( m => m.OfflineVerificationPageModule)
  },
  {
    path: 'chapter-list/:subjectId/:subjectName',
    loadChildren: () => import('./home/chapter-list/chapter-list.module').then( m => m.ChapterListPageModule)
  },
  {
     path: 'test/:id',
     loadChildren: () => import('./home/test/test.module').then( m => m.TestPageModule)
  },
  {
    path: 'bookmark-chapters/:subjectId/:subjectName',
    loadChildren: () => import('./home/bookmark-chapters/bookmark-chapters.module').then( m => m.BookmarkChaptersPageModule)
  },
  {
    path: 'notes-chapters/:subjectId/:subjectName',
    loadChildren: () => import('./home/notes-chapters/notes-chapters.module').then( m => m.NotesChaptersPageModule)
  },
  {
    path: 'question-panel',
    loadChildren: () => import('./components/question-panel/question-panel.module').then( m => m.QuestionPanelPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
