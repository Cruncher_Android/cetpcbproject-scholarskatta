import { TestBed } from '@angular/core/testing';

import { ViewTestInfoService } from './view-test-info.service';

describe('ViewTestInfoService', () => {
  let service: ViewTestInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewTestInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
