import { TestBed } from '@angular/core/testing';

import { QuestionPaperPcbInfoService } from './question-paper-pcb-info.service';

describe('QuestionPaperPcbInfoService', () => {
  let service: QuestionPaperPcbInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionPaperPcbInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
