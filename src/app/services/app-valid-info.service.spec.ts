import { TestBed } from '@angular/core/testing';

import { AppValidInfoService } from './app-valid-info.service';

describe('AppValidInfoService', () => {
  let service: AppValidInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppValidInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
