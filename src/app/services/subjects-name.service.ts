import { Injectable } from '@angular/core';
import { DatabaseServiceService } from './database-service.service';

@Injectable({
  providedIn: 'root'
})
export class SubjectsNameService {

  constructor(private databaseServiceService: DatabaseServiceService) { }

  createTable() {
    this.databaseServiceService.getDatabaseState().subscribe( ready => {
      if (ready) {
        this.databaseServiceService.getDataBase().executeSql(`CREATE TABLE IF NOT EXISTS subjects_Name 
        (QUESTION_ID integer primary key autoincrement, SUBJECT_ID integer NOT NULL, CHAPTER_ID integer NOT NULL)`, []);
      }
    });
  }
}
