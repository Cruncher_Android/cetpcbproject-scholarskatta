import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OfflineVerificationPageRoutingModule } from './offline-verification-routing.module';

import { OfflineVerificationPage } from './offline-verification.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OfflineVerificationPageRoutingModule
  ],
  declarations: [OfflineVerificationPage]
})
export class OfflineVerificationPageModule {}
