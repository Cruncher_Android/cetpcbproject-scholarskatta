import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookmarkChaptersPage } from './bookmark-chapters.page';

const routes: Routes = [
  {
    path: '',
    component: BookmarkChaptersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookmarkChaptersPageRoutingModule {}
