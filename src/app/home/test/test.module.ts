import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestPageRoutingModule } from './test-routing.module';

import { TestPage } from './test.page';

import { TestInfoComponent } from '../../components/test-info/test-info.component'


@NgModule({ 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestPageRoutingModule,
  ],
  declarations: [TestPage, TestInfoComponent],
  entryComponents: [TestInfoComponent]
})
export class TestPageModule {}
