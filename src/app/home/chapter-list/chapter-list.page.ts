import { ChapterInfoService } from "./../../services/chapter-info.service";
import { DatabaseServiceService } from "../../services/database-service.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Storage } from "@ionic/storage";
import {
  PopoverController,
  AlertController,
  IonSlides,
  Platform,
  NavController,
} from "@ionic/angular";
import { TestTypeSelectionComponent } from "src/app/components/test-type-selection/test-type-selection.component";

@Component({
  selector: "app-chapter-list",
  templateUrl: "./chapter-list.page.html",
  styleUrls: ["./chapter-list.page.scss"],
})
export class ChapterListPage implements OnInit {
  sliderOptions = {
    initialSlide: 0,
    speed: 500,
    slidesPerView: 1,
  };

  selectedSlide: any;
  selectedView = 0;

  subjectId: string;
  subjectName: string;

  chapterList: {
    chapterId: number;
    chapterName: string;
    checked: boolean;
  }[] = [];
  phyChapterList: {
    chapterId: number;
    chapterName: string;
    checked: boolean;
  }[] = [];
  chemChapterList: {
    chapterId: number;
    chapterName: string;
    checked: boolean;
  }[] = [];
  biologyChapterList: {
    chapterId: number;
    chapterName: string;
    checked: boolean;
  }[] = [];

  phyLength: number;
  chemLength: number;
  biologyLength: number;
  chaptersLength: number;

  selectedChapters = [];
  phySelectedChapters = [];
  chemSelectedChapters = [];
  biologySelectedChapters = [];

  totalChapters: number = -1;
  phyTotalChapters: number = -1;
  chemTotalChapters: number = -1;
  biologyTotalChapters: number = -1;

  checked: boolean = false;

  selectedAll: boolean = false;
  phySelectedAll: boolean = false;
  chemSelectedAll: boolean = false;
  biologySelectedAll: boolean = false;

  phySelectedAllChecked: boolean = false;
  chemSelectedAllChecked: boolean = false;
  biologySelectedAllChecked: boolean = false;

  loggedInType: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private databaseService: DatabaseServiceService,
    private chapterInfoService: ChapterInfoService,
    private storage: Storage,
    private router: Router,
    private popOverController: PopoverController,
    private alertController: AlertController,
    private platform: Platform,
    private navController: NavController
  ) {
    // console.log("chapter page loaded");
  }

  ngOnInit() {
    this.storage.get("userLoggedIn").then((data) => {
      this.loggedInType = data;
    });
    this.activatedRoute.paramMap.subscribe((subName) => {
      this.subjectId = subName.get("subjectId");
      this.subjectName = subName.get("subjectName");
      // console.log(this.subjectId);
      // console.log(this.subjectName);
      if (this.subjectId == "5") {
        this.getPhyChapters(1);
        this.getChemChapters(2);
        this.getBiologyChapters(4);
      } else {
        this.getChapters(this.subjectId);
      }
    });
  }

  async onViewChange(event) {
    // console.log("in view change");
    await this.selectedSlide.slideTo(this.selectedView);
    // console.log("selectedView", this.selectedView);
    if (this.selectedView == 0) {
      if (
        this.phySelectedChapters.length == this.phyTotalChapters &&
        this.phySelectedAll == true
      ) {
        this.phySelectedAllChecked = true;
      } else {
        this.phySelectedAllChecked = false;
      }
      // this.getPhyChapters(1);
    }

    if (this.selectedView == 1) {
      if (
        this.chemSelectedChapters.length == this.chemTotalChapters &&
        this.chemSelectedAll == true
      ) {
        this.chemSelectedAllChecked = true;
      } else {
        this.chemSelectedAllChecked = false;
      }
      // this.getChemChapters(2);
    }

    if (this.selectedView == 2) {
      if (
        this.biologySelectedChapters.length == this.biologyTotalChapters &&
        this.biologySelectedAll == true
      ) {
        this.biologySelectedAllChecked = true;
      } else {
        this.biologySelectedAllChecked = false;
      }
      // this.getMathsChapters(3);
    }
  }

  getChapters(subjectId) {
    this.chapterInfoService.selectChapter(subjectId).then((results) => {
      results.map((result) => {
        this.chapterList.push({
          chapterId: result.chapter_id,
          chapterName: result.chapter_name,
          checked: false,
        });
      });

      // console.log(this.chapterList);
      this.chaptersLength = this.chapterList.length;
      // console.log(this.chapterList.length);
    });
  }

  getPhyChapters(id) {
    if (this.phyChapterList.length == 0) {
      this.chapterInfoService.selectChapter(id).then((result) => {
        for (let i = 0; i < result.length; i++) {
          this.phyChapterList.push({
            chapterId: result[i].chapter_id,
            chapterName: result[i].chapter_name,
            checked: false,
          });
        }
        // console.log("phy chapter list", this.phyChapterList);
        this.phyLength = this.phyChapterList.length;
      });
    }
  }

  getChemChapters(id) {
    if (this.chemChapterList.length == 0) {
      this.chapterInfoService.selectChapter(id).then((result) => {
        for (let i = 0; i < result.length; i++) {
          this.chemChapterList.push({
            chapterId: result[i].chapter_id,
            chapterName: result[i].chapter_name,
            checked: false,
          });
        }
        // console.log("chem chapter list", this.chemChapterList);
        this.chemLength = this.chemChapterList.length;
      });
    }
  }

  getBiologyChapters(id) {
    if (this.biologyChapterList.length == 0) {
      this.chapterInfoService.selectChapter(id).then((result) => {
        for (let i = 0; i < result.length; i++) {
          this.biologyChapterList.push({
            chapterId: result[i].chapter_id,
            chapterName: result[i].chapter_name,
            checked: false,
          });
        }
        // console.log("biology chapter list", this.biologyChapterList);
        this.biologyLength = this.biologyChapterList.length;
      });
    }
  }

  onSelectAll() {
    if (
      this.selectedChapters.length == this.totalChapters &&
      this.selectedAll == true
    ) {
      this.selectedChapters = [];
      this.totalChapters = -1;
      this.chapterList.map((chapter) => (chapter.checked = false));
      this.selectedAll = false;
    } else {
      this.selectedChapters = [];
      for (let i = 0; i < this.chapterList.length; i++) {
        this.selectedChapters.push(this.chapterList[i].chapterId);
      }
      this.totalChapters = this.selectedChapters.length;
      this.chapterList.map((chapter) => (chapter.checked = true));
      this.selectedAll = true;
    }
    // console.log("selectedChapters", this.selectedChapters);
    // console.log("total chapters", this.totalChapters);
  } 
 
  onPhySelectAll() {
    if (
      this.phySelectedChapters.length == this.phyTotalChapters &&
      this.phySelectedAll == true
    ) {
      this.phySelectedChapters = [];
      this.phyTotalChapters = -1;
      for (let i = 0; i < this.phyChapterList.length; i++) {
        this.phyChapterList[i].checked = false;
      }
      this.phySelectedAll = false;
      //this.disableButton = true;
    } else {
      this.phySelectedChapters = [];
      for (let i = 0; i < this.phyChapterList.length; i++) {
        this.phySelectedChapters.push(this.phyChapterList[i].chapterId);
      }
      this.phyTotalChapters = this.phySelectedChapters.length;
      for (let i = 0; i < this.phyChapterList.length; i++) {
        this.phyChapterList[i].checked = true;
      }
      this.phySelectedAll = true;
      //this.disableButton = false;
    }
    // console.log("physics selectedChapters", this.phySelectedChapters);
    // console.log("physics total chapters", this.phyTotalChapters);
  }

  onChemSelectAll() {
    if (
      this.chemSelectedChapters.length == this.chemTotalChapters &&
      this.chemSelectedAll == true
    ) {
      this.chemSelectedChapters = [];
      this.chemTotalChapters = -1;
      for (let i = 0; i < this.chemChapterList.length; i++) {
        this.chemChapterList[i].checked = false;
      }
      this.chemSelectedAll = false;
      //this.disableButton = true;
    } else {
      this.chemSelectedChapters = [];
      for (let i = 0; i < this.chemChapterList.length; i++) {
        this.chemSelectedChapters.push(this.chemChapterList[i].chapterId);
      }
      this.chemTotalChapters = this.chemSelectedChapters.length;
      for (let i = 0; i < this.chemChapterList.length; i++) {
        this.chemChapterList[i].checked = true;
      }
      this.chemSelectedAll = true;
      // this.disableButton = false;
    }
    // console.log("chemistry selectedChapters", this.chemSelectedChapters);
    // console.log("chemistry total chapters", this.chemTotalChapters);
  }

  onBiologySelectAll() {
    if (
      this.biologySelectedChapters.length == this.biologyTotalChapters &&
      this.biologySelectedAll == true
    ) {
      this.biologySelectedChapters = [];
      this.biologyTotalChapters = -1;
      for (let i = 0; i < this.biologyChapterList.length; i++) {
        this.biologyChapterList[i].checked = false;
      }
      this.biologySelectedAll = false;
      //this.disableButton = true;
    } else {
      this.biologySelectedChapters = [];
      for (let i = 0; i < this.biologyChapterList.length; i++) {
        this.biologySelectedChapters.push(this.biologyChapterList[i].chapterId);
      }
      this.biologyTotalChapters = this.biologySelectedChapters.length;
      for (let i = 0; i < this.biologyChapterList.length; i++) {
        this.biologyChapterList[i].checked = true;
      }
      this.biologySelectedAll = true;
      // this.disableButton = false;
    }
    // console.log("biology selectedChapters", this.biologySelectedChapters);
    // console.log("biology total chapters", this.biologyTotalChapters);
  }

  onClick(id, index) {
    if (this.loggedInType == "demo" && index > 1) {
      this.chapterList.find(({ chapterId }) => chapterId === id).checked = true;
      this.createAlertForDemo("Please logout and register from login page to get complete access", id, this.chapterList);
      return;
    }

    if (this.selectedChapters.includes(id)) {
      this.selectedChapters.splice(this.selectedChapters.indexOf(id), 1);
      if (this.selectedChapters.length == 0) {
        this.selectedAll = false;
        // this.disableButton = true;
      }
      if (this.totalChapters == 1) {
        this.totalChapters = this.totalChapters - 2;
      } else {
        this.totalChapters = this.totalChapters - 1;
      }
    } else {
      this.selectedChapters.push(id);
      // this.disableButton = false;
      if (this.totalChapters == -1) {
        this.totalChapters = this.totalChapters + 2;
      } else {
        this.totalChapters = this.totalChapters + 1;
      }
    }
    // console.log("chapters", this.selectedChapters);
    // console.log("total chapters", this.totalChapters);
  }

  onPhyClick(id, index) {
    if (this.loggedInType == "demo" && index > 1) {
      this.phyChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = true;
      this.createAlertForDemo("Please logout and register from login page to get complete access", id, this.phyChapterList);
      return;
    }
    if (this.phySelectedChapters.includes(id)) {
      this.phySelectedChapters.splice(this.phySelectedChapters.indexOf(id), 1);
      this.phyChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = false;
      if (this.phySelectedChapters.length == 0) {
        this.phySelectedAll = false;
        //  this.disableButton = true;
      }
      if (this.phyTotalChapters == 1) {
        this.phyTotalChapters = this.phyTotalChapters - 2;
      } else {
        this.phyTotalChapters = this.phyTotalChapters - 1;
      }
    } else {
      this.phySelectedChapters.push(id);
      this.phyChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = true;
      // this.disableButton = false;
      if (this.phyTotalChapters == -1) {
        this.phyTotalChapters = this.phyTotalChapters + 2;
      } else {
        this.phyTotalChapters = this.phyTotalChapters + 1;
        if (this.phyTotalChapters == this.phyLength) {
          this.phySelectedAllChecked = true;
        }
      }
    }
    // console.log("physics selectedChapters", this.phySelectedChapters);
    // console.log("physics total chapters", this.phyTotalChapters);
  }

  onChemClick(id, index) {
    if (this.loggedInType == "demo" && index > 1) {
      this.chemChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = true;
      this.createAlertForDemo("Please logout and register from login page to get complete access", id, this.chemChapterList);
      return;
    }
    if (this.chemSelectedChapters.includes(id)) {
      this.chemSelectedChapters.splice(
        this.chemSelectedChapters.indexOf(id),
        1
      );
      this.chemChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = false;
      if (this.chemSelectedChapters.length == 0) {
        //  this.disableButton = true;
      }
      if (this.chemTotalChapters == 1) {
        this.chemTotalChapters = this.chemTotalChapters - 2;
      } else {
        this.chemTotalChapters = this.chemTotalChapters - 1;
      }
    } else {
      this.chemSelectedChapters.push(id);
      this.chemChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = true;
      // this.disableButton = false;
      if (this.chemTotalChapters == -1) {
        this.chemTotalChapters = this.chemTotalChapters + 2;
      } else {
        this.chemTotalChapters = this.chemTotalChapters + 1;
        if (this.chemTotalChapters == this.chemLength) {
          this.chemSelectedAllChecked = true;
        }
      }
    }
    // console.log("chemistry selectedChapters", this.chemSelectedChapters);
    // console.log("chemistry total chapters", this.chemTotalChapters);
  }

  onBiologyClick(id, index) {
    if (this.loggedInType == "demo" && index > 1) {
      this.biologyChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = true;
      this.createAlertForDemo("Please logout and register from login page to get complete access", id, this.biologyChapterList);
      return;
    }
    if (this.biologySelectedChapters.includes(id)) {
      this.biologySelectedChapters.splice(
        this.biologySelectedChapters.indexOf(id),
        1
      );
      this.biologyChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = false;
      if (this.biologySelectedChapters.length == 0) {
        //  this.disableButton = true;
      }
      if (this.biologyTotalChapters == 1) {
        this.biologyTotalChapters = this.biologyTotalChapters - 2;
      } else {
        this.biologyTotalChapters = this.biologyTotalChapters - 1;
      }
    } else {
      this.biologySelectedChapters.push(id);
      this.biologyChapterList.find(
        ({ chapterId }) => chapterId === id
      ).checked = true;
      // this.disableButton = false;
      if (this.biologyTotalChapters == -1) {
        this.biologyTotalChapters = this.biologyTotalChapters + 2;
      } else {
        this.biologyTotalChapters = this.biologyTotalChapters + 1;
        if (this.biologyTotalChapters == this.biologyLength) {
          this.biologySelectedAllChecked = true;
        }
      }
    }
    // console.log("biology selectedChapters", this.biologySelectedChapters);
    // console.log("biology total chapters", this.biologyTotalChapters);
  }

  async onSubmit() {
    this.storage.ready().then((ready) => {
      if (ready) {
        if (
          this.subjectId != "5" &&
          this.chaptersLength == this.totalChapters
        ) {
          this.storage.set("subject-test", true);
        } else {
          this.storage.set("subject-test", false);
        }
      }
    });
    if (
      this.subjectId == "5" &&
      this.phySelectedChapters.length != 0 &&
      this.chemSelectedChapters.length != 0 &&
      this.biologySelectedChapters.length != 0
    ) {
      this.selectedChapters = this.phySelectedChapters.concat(
        this.chemSelectedChapters.concat(this.biologySelectedChapters)
      );
    } else if (this.subjectId == "5") {
      this.selectedChapters = [];
    }
    //   else if(this.subjectId == '4') {
    //     const alert = await this.alertController.create({
    //       message: 'any',
    //       animated: true
    //   })
    //     await alert.present();
    // }
    if (this.selectedChapters.length > 0) {
      const popOver = await this.popOverController.create({
        component: TestTypeSelectionComponent,
        cssClass: "myPopOver",
        animated: true,
        backdropDismiss: false,
        componentProps: {
          chapters: this.selectedChapters,
          subjectName: this.subjectName,
        },
      });
      await popOver.present();
      popOver.onDidDismiss().then((_) => {
        // this.backdrop = false;
        this.storage.set("testTypeSelectionPopover", false);
      });
    } else {
      if (this.subjectId == "5") {
        this.createAlert("Select at least 1 chapter from each subject");
      } else {
        this.createAlert("Select at least 1 chapter");
      }
    }
  }

  async createAlert(message) {
    const alert = await this.alertController.create({
      subHeader: message,
      cssClass: "alert-title",
      animated: true,
      buttons: [
        {
          role: "cancel",
          text: "Ok",
        },
      ],
    });
    await alert.present();
  }

  async createAlertForDemo(message, id, chaptersArray) {
    const alert = await this.alertController.create({
      subHeader: message,
      cssClass: "alert-title",
      animated: true,
      buttons: [
        {
          text: "Ok",
          handler: () => {
            chaptersArray.find(
              ({ chapterId }) => chapterId === id
            ).checked = false;
          },
        },
      ],
      backdropDismiss: false
    });
    await alert.present();
  }

  onBackClick() {
    this.popOverController.dismiss();
  }

  async slideChanged(slides: IonSlides) {
    // console.log("in slide change");
    //  this.slider.getActiveIndex().then(index => {
    //   this.selectedView = index
    //   })
    //   console.log(this.selectedView)
    this.selectedSlide = slides;
    slides.getActiveIndex().then((index) => {
      this.selectedView = index;
    });
  }
}
