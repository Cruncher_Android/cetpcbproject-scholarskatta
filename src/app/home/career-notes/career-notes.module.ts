import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CareerNotesPageRoutingModule } from './career-notes-routing.module';

import { CareerNotesPage } from './career-notes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CareerNotesPageRoutingModule
  ],
  declarations: [CareerNotesPage]
})
export class CareerNotesPageModule {}
